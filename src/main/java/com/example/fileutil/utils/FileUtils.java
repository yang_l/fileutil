package com.example.fileutil.utils;


import lombok.extern.slf4j.Slf4j;
import org.springframework.util.ObjectUtils;

import java.io.*;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author YangLi
 * @Date 2025/2/20 15:35
 * @注释 文件工具类
 */
@Slf4j
public class FileUtils {

    /**
     * 读取文本文件内容为字符串
     *
     * @param file 需要读取的文件
     * @return 文件字符串
     * @throws IOException ·
     */
    public static String readFileToString(File file) throws IOException {
        StringBuilder content = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = reader.readLine()) != null) {
                content.append(line).append(System.lineSeparator());
            }
        }
        return content.toString();
    }

    /**
     * 读取文件内容为字节数组
     *
     * @param file 需要读取的文件
     * @return 字节数组
     * @throws IOException ·
     */
    public static byte[] readFileToByteArray(File file) throws IOException {
        return Files.readAllBytes(file.toPath());
    }

    /**
     * 将字符串写入文本文件
     *
     * @param file 需要读取的文件
     * @throws IOException ·
     */
    public static void writeStringToFile(File file, String data) throws IOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            writer.write(data);
        }
    }

    /**
     * 将字节数组写入文件
     *
     * @param file 要写入的文件
     * @param data 要写入的数组
     * @throws IOException ·
     */
    public static void writeByteArrayToFile(File file, byte[] data) throws IOException {
        Files.write(file.toPath(), data);
    }

    /**
     * 复制文件
     *
     * @param source      源文件
     * @param destination 新文件
     * @throws IOException ·
     */
    public static void copyFile(File source, File destination) throws IOException {
        Files.copy(source.toPath(), destination.toPath(), StandardCopyOption.REPLACE_EXISTING);
    }

    /**
     * 删除文件或目录
     *
     * @param file 需要删除的文件或目录地址
     * @return 成功 返回 true 失败返回 false
     * @throws IOException ·
     */
    public static Boolean delete(File file) throws IOException {
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            if (!ObjectUtils.isEmpty(files)) {
                boolean flag = true;
                for (File item : files) {
                    if (!item.delete()) {
                        flag = !item.delete();
                    }
                }
                return flag;
            }
            // 删除文件夹
            return file.delete();
        } else {
            // 删除文件
            return Files.deleteIfExists(file.toPath());
        }
    }

    /**
     * 遍历目录，获取所有文件
     *
     * @param directory 需要便利的文件夹地址
     * @return 文件夹下的所有文件
     */
    public static List<File> listFiles(File directory) {
        List<File> fileList = new ArrayList<>();
        if (directory.isDirectory()) {
            File[] files = directory.listFiles();
            if (files != null) {
                for (File file : files) {
                    if (file.isDirectory()) {
                        fileList.addAll(listFiles(file));
                    } else {
                        fileList.add(file);
                    }
                }
            }
        }
        return fileList;
    }

    /**
     * 文件名重命名
     *
     * @param path        文件的上一层目录路径
     * @param oldFileName 要修改的文件旧路径
     * @param newFileName 新的文件路径
     * @return 成功返回true, 失败返回false
     */
    public static boolean renameFile(String path, String oldFileName, String newFileName) {
        // 新的文件名和以前文件名不同时,才有必要进行重命名
        if (!oldFileName.equals(newFileName)) {
            String oldPathName = path + File.separator + oldFileName;
            File oldFile = new File(oldPathName);
            String newPathName = path + File.separator + newFileName;
            File newFile = new File(newPathName);
            // 若在该目录下已经有一个文件和新文件名相同，则不允许重命名
            if (newFile.exists()) {
                return false;
            } else {
                return oldFile.renameTo(newFile);
            }
        }
        return true;
    }

    /**
     * 移动文件
     *
     * @param filename 文件名
     * @param oldPath  旧文件地址
     * @param newPath  新文件地址
     * @param cover    存在同名文件true:覆盖；false:不覆盖
     */
    public Boolean changeDirectory(String filename, String oldPath, String newPath, boolean cover) {
        if (!oldPath.equals(newPath)) {
            String oldPathName = oldPath + File.separator + filename;
            File oldFile = new File(oldPathName);
            String newPathName = newPath + File.separator + filename;
            File newFile = new File(newPathName);
            // 若在待转移目录下，已经存在待转移文件
            if (newFile.exists()) {
                if (cover) {
                    // 覆盖
                    return oldFile.renameTo(newFile);
                } else {
                    return false;
                }
            } else {
                return oldFile.renameTo(newFile);
            }
        }
        return true;
    }

    /**
     * 获取文件大小
     *
     * @param file 需要获取大小的文件
     * @return 文件大小
     */
    public static long getFileSize(File file) {
        return file.length();
    }

    /**
     * 获取文件最后修改时间
     *
     * @param file 需要读取的文件
     * @return 时间 long 类型的
     */
    public static long getLastModifiedTime(File file) {
        return file.lastModified();
    }

    /**
     * 检查文件或目录是否存在
     *
     * @param file 需要读取的文件
     * @return 存在 返回 true 不存在返回 false
     */
    public static boolean exists(File file) {
        return file.exists();
    }

    /**
     * 创建新文件
     *
     * @param file 文件完整路径  带名字
     * @return 成功 true 失败 false
     * @throws IOException ·
     */
    public static boolean createNewFile(File file) throws IOException {
        return file.createNewFile();
    }

    /**
     * 创建目录
     *
     * @param directory 文件夹路径
     * @return 成功 true 失败 false
     */
    public static boolean createDirectory(File directory) {
        return directory.mkdirs();
    }
}

