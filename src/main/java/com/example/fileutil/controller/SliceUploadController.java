package com.example.fileutil.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author YangLi
 * @Date 2024/2/2 10:02
 * @注释
 */
@RestController
@Slf4j
public class SliceUploadController {


    @Value("${file.uploadPath}")
    private String uploadPath;

    private static final ConcurrentHashMap<String, List<Integer>> sliceMap = new ConcurrentHashMap<>();

    private static final Integer ZERO = 0;

    /**
     * 分片上传
     *
     * @param chunkSize   每个分片大小
     * @param chunkNumber 当前分片
     * @param md5         文件总MD5
     * @param file        当前分片文件数据
     * @return `
     * @throws IOException `
     */
    @RequestMapping("/uploadBig")
    public ResponseEntity<Map<String, String>> uploadBig(@RequestParam Long chunkSize,       // 当前片大小
                                                         @RequestParam Integer totalNumber,  // 总片数
                                                         @RequestParam Integer chunkNumber,  // 当前片
                                                         @RequestParam String md5,
                                                         @RequestParam MultipartFile file) throws IOException {
        log.info("大文件上传");
        //  原理多线程并发上传文件  首先得有一个全局的 map sliceMap  我们先搞一个在上面
        //  然后我们在这个map中采用<String, list<Integer>>  数据结构  然后创建一个  totalNumber 大小的list 每一个位置上都是  0  表示 还没有进行分片上传
        //  为了让每一个key唯一 我们使用MD5作为key值， 使用这个方法让其是一个原子操作  避免线程不安全出现
        //  然后我们每写入一片数据就将 list中对应位置上的值更新为1 表示这一片已经上传过了
        //  这样在上传某一片的时候  我们也可以对list中对应位置的值进行判断 如果是1了  就不用再进行上传了
        //  最后判断list中的所有位置上的值都是1的情况就表示 所有的片都已经上传完成了
        if (sliceMap.get(md5) == null) {
            ArrayList<Integer> list = new ArrayList<>(totalNumber);
            for (int i = 0; i < totalNumber; i++) list.add(i, 0);
            sliceMap.putIfAbsent(md5, list);
        }

        //文件存放位置
        String dstFile = String.format("%s\\%s", uploadPath, file.getOriginalFilename());
        //上传分片信息存放位置
        String confFilePath = String.format("%s\\%s\\%s.conf", uploadPath, md5, md5);
        // 这里创建文件检查时需要用到的标识文件  之后每写入一片 都会在对应的位置写入  是否成功的标识  最主要还是创建父文件目录
        createFileWithLocationInformation(confFilePath);

        List<Integer> list = sliceMap.get(md5);

        //随机分片写入文件
        try (RandomAccessFile randomAccessFile = new RandomAccessFile(dstFile, "rw");
             RandomAccessFile randomAccessConfFile = new RandomAccessFile(confFilePath, "rw");
             InputStream inputStream = file.getInputStream()) {
            if (ZERO.equals(list.get(chunkNumber))) {
                //定位到该分片的偏移量
                randomAccessFile.seek(chunkNumber * chunkSize);
                //写入该分片数据
                randomAccessFile.write(readAllBytes(inputStream));
                //定位到当前分片状态位置
                randomAccessConfFile.seek(chunkNumber);
                //设置当前分片上传状态为1
                randomAccessConfFile.write(1);
            }
        }
        // 当list中没有0的存在时  即全部是1  就表示所有的片都传完了
        if (!list.contains(0)) {
            log.info("文件上传完成");
            // todo  业务逻辑
        }

        log.info("上传的文件为 : {}", file.getOriginalFilename());
        return ResponseEntity.ok(Collections.singletonMap("msg", dstFile));
    }

    private byte[] readAllBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        int nRead;
        byte[] data = new byte[1024];

        while ((nRead = inputStream.read(data, 0, data.length)) != -1) {
            buffer.write(data, 0, nRead);
        }

        buffer.flush();

        return buffer.toByteArray();
    }

    /**
     * 创建具有 每一片该放哪个位置 信息 的 文件
     *
     * @param filePath `文件路径
     */
    private void createFileWithLocationInformation(String filePath) {
        File dir = new File(filePath).getParentFile();
        if (!dir.exists()) {
            if (!dir.mkdirs()) {
                log.info("文件创建失败");
            }
            // 在这个文件写入位置信息   相当与键入一个空文件 ，文件大小为 bytesSize 这么多字节  方便后续 每一片随机写入的时候找到自己的位置
            byte[] bytes = new byte[1];
            // 这一步是在文件上追加上面这么大小的空白bytes
            try {
                Files.write(Paths.get(filePath), bytes);
            } catch (IOException e) {
                e.printStackTrace();
                log.info("文件写入位置信息异常");
            }
        }
    }

    /**
     * 获取文件分片状态，检测文件MD5合法性
     *
     * @param md5 `
     * @return map
     * @throws Exception `
     */
    @PostMapping("/checkFile")
    public ResponseEntity<Map<String, String>> uploadBig(@RequestParam String md5, String fileName) throws Exception {
        log.info("检测文件MD5合法性");

        String uploadPathNew = String.format("%s\\%s\\%s.conf", uploadPath, md5, md5);
        Path path = Paths.get(uploadPath);
        //MD5目录不存在文件从未上传过
        if (!Files.exists(path.getParent())) {
            return ResponseEntity.ok(Collections.singletonMap("msg", "文件未上传"));
        }
        //判断文件是否上传成功
        StringBuilder stringBuilder = new StringBuilder();
        byte[] bytes = Files.readAllBytes(path);
        for (byte b : bytes) {
            stringBuilder.append(String.valueOf(b));
        }
        //所有分片上传完成计算文件MD5
        HashMap<String, String> map = new HashMap<>();
        File file = new File(String.format("%s\\%s", uploadPathNew, fileName));
        //计算文件MD5是否相等
        String filePath = file.getAbsolutePath();
        if (!stringBuilder.toString().contains("0")) {
            try (InputStream inputStream = new FileInputStream(file)) {
                String md5pwd = DigestUtils.md5DigestAsHex(inputStream);
                if (!md5pwd.equalsIgnoreCase(md5)) {
                    return ResponseEntity.ok(Collections.singletonMap("msg", "文件未上传"));
                }
            }
            map.put("path", filePath + "  文件合法");
        } else {
            //文件未上传完成，反回每个分片状态，前端将未上传的分片继续上传
            map.put("path", filePath + "  文件不合法");
        }
        map.put("chucks", stringBuilder.toString());
        return ResponseEntity.ok(map);

    }

    // 原文连接 ： https://juejin.cn/post/7266265543412351030

}